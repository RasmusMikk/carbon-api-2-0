CREATE TABLE `record` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `url` varchar(2000) NOT NULL DEFAULT '',
  `bytes` int(50) DEFAULT NULL,
  `green` tinyint(1) DEFAULT NULL,
  `co2` decimal(18,16) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;
